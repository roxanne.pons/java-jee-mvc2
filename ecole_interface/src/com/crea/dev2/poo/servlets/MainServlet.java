package com.crea.dev2.poo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crea.dev2.poo.beans.Chambre;
import com.crea.dev2.poo.beans.Eleve;
import com.crea.dev2.poo.dao.ChambreDAO;
import com.crea.dev2.poo.dao.EleveDAO;

/**
 * Servlet implementation class PremiereServlet
 */
@WebServlet( name = "front_controller", urlPatterns = "/home")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private void addEleve(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Recupère les donnees du formulaire
		String nom = request.getParameter("nomelev"); 
		int age = Integer.parseInt(request.getParameter("agelev")); 
		String adresse = request.getParameter("adresselev"); 
		String num = request.getParameter("numelev");
		
		// Appelle le DAO
		int code =  EleveDAO.addEleve(num, nom, age, adresse);
		
		// Redirection
		response.sendRedirect("home?idaction=eleves-list");
	}
	
	private void getEleveByNum(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
		//recup le param numelev
		String numelev = request.getParameter("numelev");
		//recup l'eleve a partir du dao
		Eleve erecup =  EleveDAO.getEleveByNum(numelev);
		//forward vers la vue correspondante apres y avoir passé l'attribut
		request.setAttribute("eleve", erecup);
		request.getRequestDispatcher("/eleveByNum.jsp").forward(request, response);	
	}

	private void listerEleves(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		request.setAttribute("eleves", EleveDAO.getAllEleve() );// Permettre d'utiliser "eleves" dans la JSP
		this.getServletContext().getRequestDispatcher("/eleves-list.jsp").forward(request, response);
	}
	
	//-------------------------CHAMBRES
	
	private void addChambre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Recupère les donnees du formulaire
		String num = request.getParameter("numchambre"); 
		int no = Integer.parseInt(request.getParameter("nochambre")); 
		float prix = Integer.parseInt(request.getParameter("prixchambre")); 

		// Appelle le DAO
		int code =  ChambreDAO.addChambre(no, num, prix);
		
		// Redirection
		response.sendRedirect("home?idaction=chambres-list");
	}
	
	private void getChambreByNo(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
		//recup le param numelev
		int nochambre = Integer.parseInt(request.getParameter("nochambre"));
		//recup l'eleve a partir du dao
		Chambre chrecup =  ChambreDAO.getChambreByNo(nochambre);
		//forward vers la vue correspondante apres y avoir passé l'attribut
		request.setAttribute("chambre", chrecup);
		request.getRequestDispatcher("/chambreByNo.jsp").forward(request, response);	
	}
	
	
	private void listerChambres(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		request.setAttribute("chambres", ChambreDAO.getAllChambre() );// Permettre d'utiliser "eleves" dans la JSP
		this.getServletContext().getRequestDispatcher("/chambres-list.jsp").forward(request, response);
	}
	
	
	/**
	 * @throws IOException 
	 * @throws ServletException 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// Détermine l'action a exécuter et exécute cette action ( front controller )
			String idaction = request.getParameter("idaction"); //a partir de chaque formulaire HTML/XHTML/JSTL/JSP
			if( idaction==null ) {
				request.getRequestDispatcher("/index.jsp").forward(request, response);
				return;
			}
			switch(idaction) {
				case "eleves-list":
					this.listerEleves(request, response);
					break;
				case "addEleve":
					this.addEleve(request, response);
					break;
				case "getEleveByNum":
					this.getEleveByNum(request, response);
					break;
				case "chambres-list":
					this.listerChambres(request, response);
					break;
				case "addChambre":
					this.addChambre(request, response);
					break;
				case "getChambreByNo":
					this.getChambreByNo(request, response);
					break;

				default:
					throw new RuntimeException("Cette action n'existe pas");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste eleves</title>
</head>
<body>
	<div align="center">
		<c:import url="_MENU.jsp"></c:import>
	</div>
	
	<h1>Liste eleves</h1>
	<table>
		<thead>
			<th>Num</th>
			<th>No</th>
			<th>Nom</th>
			<th>Age</th>
			<th>Adresse</th>
			<th>Actions</th>	
		</thead>
		<tbody>
				<c:forEach items="${eleves}" var="eleve">
					<tr>
						<td>${ eleve.num }</td>
						<td>${ eleve.no }</td>
						<td>${ eleve.nom }</td>
						<td>${ eleve.age }</td>
						<td>${ eleve.adresse }</td>
						<td>SUPPRIMER / EDITER</td>
					</tr>
				</c:forEach>
		</tbody>
	</table>
	
	<h2>Ajouter un nouvel élève</h2>
	<form name="id" class="form" action="<c:url value="/home"/>?idaction=addEleve" method="POST">
			<table>
				<tr>
					<td width="120px">Numero ID :</td>
					<td><input type="text" name="numelev" id="numelev" /></td>
										<td width="120px">Nom :</td>
					<td><input type="text" name="nomelev" id="nomelev" /></td>

										<td width="120px">Age :</td>
					<td><input type="number" name="agelev" id="agelev" /></td>

										<td width="120px">Adresse :</td>
					<td><input type="text" name="adresselev" id="adresselev" /></td>

				
					<td width="120px"><input type="submit" value="Valider"
						class="submit" /></td>
			</table>
	</form>
	<h2>Rechercher un élève</h2>
	<form name="id" class="form" action="<c:url value="/home"/>?idaction=getEleveByNum" method="POST">
			<table>
				<tr>
					<td width="120px">Numero ID :</td>
					<td><input type="text" name="numelev" id="numelev" /></td>

				
					<td width="120px"><input type="submit" value="Valider"
						class="submit" /></td>
			</table>
	</form>
	
</body>
</html>
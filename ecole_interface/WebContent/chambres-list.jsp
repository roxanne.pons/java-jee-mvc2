<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste chambres</title>
</head>
<body>
	<div align="center">
		<c:import url="_MENU.jsp"></c:import>
	</div>
	
	<h1>Liste Chambres</h1>
	<table>
		<thead>
			<th>Num</th>
			<th>No</th>
			<th>Prix</th>
			<th>Actions</th>	
		</thead>
		<tbody>
				<c:forEach items="${chambres}" var="chambre">
					<tr>
						<td>${ chambre.num }</td>
						<td>${ chambre.no }</td>
						<td>${ chambre.prix }</td>
						<td>SUPPRIMER / EDITER</td>
					</tr>
				</c:forEach>
		</tbody>
	</table>
	
	<h2>Ajouter une nouvelle chambre</h2>
	<form name="id" class="form" action="<c:url value="/home"/>?idaction=addChambre" method="POST">
			<table>
				<tr>
					<td width="120px">num :</td>
					<td><input type="text" name="numchambre" id="numchambre" /></td>
					<td width="120px">No :</td>
					<td><input type="number" name="nochambre" id="nochambre" /></td>

					<td width="120px">Prix :</td>
					<td><input type="number" name="prixchambre" id="prixchambre" /></td>
				
					<td width="120px"><input type="submit" value="Valider"
						class="submit" /></td>
			</table>
	</form>
	
	<h2>Rechercher une chambre</h2>
	<form name="id" class="form" action="<c:url value="/home"/>?idaction=chambreByNo" method="POST">
			<table>
				<tr>
					<td width="120px">No :</td>
					<td><input type="number" name="nochambre" id="nochambre" /></td>

				
					<td width="120px"><input type="submit" value="Valider"
						class="submit" /></td>
			</table>
	</form>
	
</body>
</html>
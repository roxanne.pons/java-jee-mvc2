<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<nav>
	<a href='<c:url value="/home?idaction=eleves-list"></c:url>'>Gestion élèves</a>
	<a href='<c:url value="/home?idaction=chambres-list"></c:url>'>Gestion chambres</a>
	<a href='<c:url value="/home?idaction=livres-list"></c:url>'>Gestion livres</a>
</nav>
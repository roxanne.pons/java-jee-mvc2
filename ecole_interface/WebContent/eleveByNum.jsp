<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Eleve</title>
</head>
<body>
<div align="center">
	<c:import url="_MENU.jsp"></c:import>
</div>
<h1>Eleve correspondant au Num: ${ eleve.num }</h1>
<p>NO: ${ eleve.no }</p>
<p>Nom: ${ eleve.nom }</p>
<p>Age: ${ eleve.age }</p>
<p>Adresse: ${ eleve.adresse }</p>

</body>
</html>
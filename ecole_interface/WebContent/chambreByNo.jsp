<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Chambre</title>
</head>
<body>
<div align="center">
	<c:import url="_MENU.jsp"></c:import>
</div>
<h1>Chambre correspondant au no: ${ chambre.no }</h1>
<p>Num: ${ chambre.num }</p>
<p>Prix: ${ chambre.prix }</p>

</body>
</html>
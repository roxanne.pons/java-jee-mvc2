package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;

import com.crea.dev2.poo.beans.Livre;
import com.crea.dev2.poo.dao.LivreDAO;
import com.crea.dev2.poo.dao.LivreDAO;

public class LivreDAOTest {

	@Test
	public void testAddLivre() throws ParseException {
		int insert = 1;
		int delete = 1;
		LivreDAO.addLivre("cotenumero1", null, "fnwerotienkd", null);
		LivreDAO.addLivre("cotenumero2", null, "gfsddnjhfgdsf", null);
		LivreDAO.addLivre("cotenumero3", null, "fhtgrdcsvghhj", null);
		LivreDAO.addLivre("cotenumero4", null, "jhtgrwfgerwer", null);
		
		LivreDAO.deleteLivreByCote("cotenumero1");
		LivreDAO.deleteLivreByCote("cotenumero2");
		LivreDAO.deleteLivreByCote("cotenumero3");
		LivreDAO.deleteLivreByCote("cotenumero4");
		LivreDAO.deleteLivreByCote("cotenumero5");
	}

}

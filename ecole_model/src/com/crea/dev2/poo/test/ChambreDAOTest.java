package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import com.crea.dev2.poo.beans.Eleve;
import com.crea.dev2.poo.beans.Chambre;
import com.crea.dev2.poo.dao.EleveDAO;
import com.crea.dev2.poo.dao.ChambreDAO;

public class ChambreDAOTest {
	
	@Before
	public void testAddListeEleveAndChambreToTest() {
		int insert = 1;
		assertEquals(insert,EleveDAO.addEleve("AGUE001", "AGUE MAX", 40, "18 Rue Labat 75018 Paris"));
		assertEquals(insert,EleveDAO.addEleve("AGUE002", "AGUE MAX", 42, "19 Rue Le Monde Paris"));
		assertEquals(insert,EleveDAO.addEleve("KAMTO005", "KAMTO Diog�ne", 50, "54 Rue des Ebisoires 78300 Poissy"));
		assertEquals(insert,EleveDAO.addEleve("LAURENCY004", "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris"));
		assertEquals(insert,EleveDAO.addEleve("TABIS003", "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris"));
		assertEquals(insert,EleveDAO.addEleve("TAHAE002", "TAHAR RIDENE", 30, "12 Rue des Chantiers 78000 Versailles"));
		assertEquals(insert,EleveDAO.addEleve("MAXI001", "MAX IBERIS", 20, "7 Rue de La Non Existance 4242 Villeneuve"));
		assertEquals(insert,EleveDAO.addEleve("MAXI002", "MAX IBERIS", 30, "8 Rue de La Non Existance 4242 Villeneuve"));
		
		assertEquals(insert, ChambreDAO.addChambre(1, "AGUE001", 400));
		assertEquals(insert, ChambreDAO.addChambre(2, "AGUE002", 500));
		assertEquals(insert, ChambreDAO.addChambre(3, "KAMTO005", 600));
		assertEquals(insert, ChambreDAO.addChambre(4, "LAURENCY004", 425.25F));
		assertEquals(insert, ChambreDAO.addChambre(5, "TABIS003", 550.50F));
		assertEquals(insert, ChambreDAO.addChambre(6, "TAHAE002", 675.75F));
	}
	
	@After
	public void testDeleteListeEleveToTest() {
		int delete = 1;
		
		assertEquals(delete, ChambreDAO.deleteChambreByNo(1));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(2));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(3));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(4));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(5));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(6));
		
		assertEquals(delete,EleveDAO.deleteEleveByNum("AGUE001"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("AGUE002"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("KAMTO005"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("LAURENCY004"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("TABIS003"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("TAHAE002"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("MAXI001"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("MAXI002"));
	}
	
	@Test
	public void testAddChambre() {
	    int insert = 1;
	    int delete = 1;
	    
	    assertEquals(insert, ChambreDAO.addChambre(7,"AGUE001", 400));
	    assertEquals(insert, ChambreDAO.addChambre(8,"AGUE002", 500.25F));
	    assertNotEquals(insert, ChambreDAO.addChambre(8,"AGUE001", 500.25F)); // à tester
	    assertEquals(delete, ChambreDAO.deleteChambreByNo(7));
	    assertEquals(delete, ChambreDAO.deleteChambreByNo(8));
	}
	
	@Test
	public void testDeleteChambreByNo() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert, ChambreDAO.addChambre(7,"AGUE001", 400));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(7));
		assertNotEquals(delete, ChambreDAO.deleteChambreByNo(7)); // si elle a bien été retirée elle ne peut pas être retirée une seconde fois
		assertNotEquals(delete, ChambreDAO.deleteChambreByNo(8)); // la chambre 8 n'existe pas, donc elle ne peut pas être retiré
	}
	
	@Test
	public void testGetChambreByNo() throws SQLException {
		 Chambre c_ref = new Chambre(1, "AGUE001", 400);
		 Chambre c_res = new Chambre();
		 c_res = ChambreDAO.getChambreByNo(1);
		 
		 assertEquals(c_ref.getNo(), c_res.getNo());
		 assertEquals(c_ref.getNum(), c_res.getNum());
		// pas possible de comparrer des float
	}
	
	@Test
	public void testGetAllEleve() throws SQLException {
		 ArrayList<Chambre> listChambre = new ArrayList<Chambre>();
		 
		 Chambre c1 = new Chambre(1, "AGUE001", 400);
		 Chambre c2 = new Chambre(2, "AGUE002", 500);
		 Chambre c3 = new Chambre(3, "KAMTO005", 600);
		 Chambre c4 = new Chambre(4, "LAURENCY004", 425.25F);
		 Chambre c5 = new Chambre(5, "TABIS003", 550.50F);
		 Chambre c6 = new Chambre(6, "TAHAE002", 675.75F);
		 
		 listChambre.add(c1);
		 listChambre.add(c2);
		 listChambre.add(c3);
		 listChambre.add(c4);
		 listChambre.add(c5);
		 listChambre.add(c6);
		 
		 for( int i =0; i < listChambre.size(); i++ )
		 {  
			 assertEquals(listChambre.get(i).getNo(), ChambreDAO.getAllChambre().get(i).getNo());
			 assertEquals(listChambre.get(i).getNum(), ChambreDAO.getAllChambre().get(i).getNum());
			 // pas possible de comparrer des float
		 }
	}
	
	@Test
	public void testupdatePrixByNum() throws SQLException {
		// encore à faire
		int insert = 1;
		int update = 1;
		int delete = 1;
		
		assertEquals(insert, ChambreDAO.addChambre(7, "MAXI001", 400));
		assertNotEquals(update, ChambreDAO.updatePrixByNum("TEST001", 500));
		assertEquals(update, ChambreDAO.updatePrixByNum("MAXI001", 500));
		assertEquals(delete, ChambreDAO.deleteChambreByNo(7));
	}
	
	@Test
	public void testupdateNumByNo() throws SQLException {
		// encore à faire
		// attention il y a deux variante de cette méthode à tester
		
		int insert = 1;
		int update = 1;
		int delete = 1;
		String newName1 = "MAXI002";
		String newNAme2 = null;
		
		//variante 1 :
		assertEquals(insert, ChambreDAO.addChambre(7, "MAXI001", 400));
		assertEquals(update, ChambreDAO.updateNumByNo("MAXI002", 7));
		assertEquals(newName1, ChambreDAO.getChambreByNo(7).getNum());
		assertEquals(delete, ChambreDAO.deleteChambreByNo(7));
		
		// variante 2 :
		assertEquals(insert, ChambreDAO.addChambre(7, "MAXI001", 400));
		assertEquals(update, ChambreDAO.updateNumByNo(7));
		assertEquals(newNAme2, ChambreDAO.getChambreByNo(7).getNum());
		assertEquals(delete, ChambreDAO.deleteChambreByNo(7));
	}
	
}

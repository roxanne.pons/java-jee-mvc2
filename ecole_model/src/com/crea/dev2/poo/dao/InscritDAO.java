package com.crea.dev2.poo.dao;

import com.crea.dev2.poo.beans.Inscrit;
import com.crea.dev2.poo.utils.DBAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class InscritDAO {

    /**
     * Liste un Inscrit en fonction de son code
     *
     * @param code code indentifiant de l'inscrit
     * @return retourne l'objet inscrit
     * @throws SQLException
     */
    public static Inscrit getInscritByCode(String code) throws SQLException {
        DBAction.DBConnexion(); // test conn
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM inscrit WHERE code = '" + code + "';");
        resultSet.first(); // Starting the resultset at the first position
        Inscrit inscritTemp = new Inscrit(resultSet.getString("code"), resultSet.getString("num"), resultSet.getFloat("note"));
        resultSet.close(); // Fermeture du resultSet
        DBAction.DBClose();
        return inscritTemp;
    }


    /**
     * Delete Inscrit par code
     *
     * @param code code de l'inscrit à supprimer
     * @return 1 ou 0 (le nbr d'inscrit supprimés) sinon le (-) code d'erreur
     */
    public static int deleteInscritByCode(String code) {
        int result; // résultat de la requête
        DBAction.DBConnexion(); // test conn
        try {
            result = DBAction.getStm().executeUpdate("DELETE FROM inscrit WHERE code = '" + code + "';");
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }

    /**
     * Ajouter un inscrit
     *
     * @param code code de l'inscrit
     * @param num  numéro de l'élève inscrit
     * @param note de l'élève
     * @return 1 ou 0 (le nbr d'uv ajouté) sinon le (-) code d'erreur
     */
    public static int addInscrit(String code, String num, float note) {
        DBAction.DBConnexion(); // test conn
        int result; //résultat de la requête
        // Attention a bien mettre des '' pour les strings
        String SQL = "INSERT INTO inscrit(code,num,note) VALUES('" + code + "','" + num + "'," + note + ");";
        try {
            result = DBAction.getStm().executeUpdate(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
            result = e.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }

    /**
     * La liste des Inscrits
     *
     * @return : la liste de tous les Inscrits.
     * @throws SQLException
     */
    public static Vector<Inscrit> getAllInscrit() throws SQLException {
        DBAction.DBConnexion(); // test conn
        Vector<Inscrit> vInscrit = new Vector<Inscrit>();
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM inscrit;");
        while (resultSet.next()) {
            Inscrit inscritTemp = new Inscrit(resultSet.getString("code"), resultSet.getString("num"), resultSet.getFloat("note"));
            vInscrit.add(inscritTemp);
        }
        DBAction.DBClose();
        return vInscrit;
    }

    /**
     * Modifier le numéro en fonction du code
     *
     * @param code code de l'inscrit à modifier
     * @param num  nouveau num
     * @return
     * @throws SQLException
     */
    public static int updateNumByCode(String code, String num) throws SQLException {
        int result;
        DBAction.DBConnexion();
        String req = "UPDATE inscrit SET num = '" + num + "' WHERE code ='" + code + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }

        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }


    /**
     * Modifie la note selon le num entré
     *
     * @param num  numéro d'identifiant de l'élève
     * @param note nouvelle note
     * @return
     * @throws SQLException
     */
    public static int updateNotebyNum(String num, float note) throws SQLException {
        int result = -1;
        DBAction.DBConnexion();
        String req = "UPDATE inscrit SET note = " + note + " WHERE num ='" + num + "' ";
        result = DBAction.getStm().executeUpdate(req);
        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }
}
